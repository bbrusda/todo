/**
 * Created by bdbtzc on 10/14/2015.
 */
var app = {}; // create namespace for our app

(function ($) {

    app.Todo = Backbone.Model.extend({
        defaults: {
            title: '',
            completed: false
        },

        toggle: function () {
            this.save({
                completed: !this.get('completed')
            });
        }
    });

    app.TodoList = Backbone.Collection.extend({
        model: app.Todo,
        localStorage: new Backbone.LocalStorage("TodoStore"),
        completed: function () {
            return this.filter(function (todo) {
                return todo.get("completed");
            });
        },

        remaining: function () {
            return this.without.apply(this, this.completed());
        }
    });

    app.todoList = new app.TodoList();
    app.TodoView = Backbone.View.extend({
        initialize: function () {
            this.model.on("change", this.render, this);
            this.model.on('destroy', this.remove, this);
        },

        tagName: "tr",
        class: "col-md-3",
        template: _.template($("#item-template").html()),
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.input = this.$(".edit");
            return this;
        },
        events: {
            'keypress .edit': 'enterKeyPress',
            'blur .edit': 'close',
            'click .toggle': 'toggleCompleted',
            'click .destroy': 'destroy'
        },

        enterKeyPress: function (e) {
            if (e.which === 13) this.close();
        },

        close: function () {
            var value = this.input.val().trim();

            if (value) {
                this.model.save({
                    title: value
                });
            }

        },

        toggleCompleted: function () {
            this.model.toggle();
            updateRemaining();
        },

        destroy: function () {
            this.model.destroy();
            updateRemaining();
        }
    });

    app.AppView = Backbone.View.extend({
        el: "#todoapp",
        initialize: function () {
            this.input = this.$("#new-todo");
            app.todoList.on("add", this.addAll, this);
            app.todoList.on("reset", this.addAll, this);
            app.todoList.fetch();
        },
        events: {
            "keypress #new-todo": "createTodoOnEnter"
        },

        createTodoOnEnter: function (e) {
            if (e.which !== 13 || !this.input.val().trim()) return;

            app.todoList.create({
                title: this.input.val().trim(),
                completed: false
            });
            this.input.val("");
        },

        addOne: function (todoModel) {
            var view = new app.TodoView({
                model: todoModel
            });
            $('#td-list').append(view.render().el);
        },

        addAll: function () {
            $('#td-list').html("");
            if (window.filter == "completed") {
                updateRemaining();
                _.each(app.todoList.completed(), this.addOne);
            }
            else if (window.filter == "todo") {
                updateRemaining();
                _.each(app.todoList.remaining(), this.addOne);
            }
            else {
                updateRemaining();
                app.todoList.each(this.addOne, this);
            }
        }
    });
    function updateRemaining(){
        if(app.todoList.remaining().length == 0){
            $('#items-todo').removeClass("status");
            $('#items-todo').addClass("status-done");
            $('#items-todo').text("All items have been compeleted!")
        }
        else {
            $('#items-todo').removeClass("status-done");
            $('#items-todo').addClass("status");
            $('#items-todo').text("Remaining ToDo Items: " + app.todoList.remaining().length);
        }
    }
    app.Router = Backbone.Router.extend({
        routes: {
            ':filter': 'setFilter'
        },
        setFilter: function (params) {
            window.filter = params.trim() || '';
            app.todoList.trigger('reset');
        }
    });

    app.router = new app.Router();
    Backbone.history.start();

    app.appView = new app.AppView();

})(jQuery);